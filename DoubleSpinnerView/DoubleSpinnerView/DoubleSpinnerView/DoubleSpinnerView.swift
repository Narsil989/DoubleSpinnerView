//
//  doubleSpinnerView.swift
//  DoubleSpinnerView
//
//  Created by Dejan Kraguljac on 25/10/2017.
//  Copyright © 2017 Dejan Kraguljac. All rights reserved.
//

import UIKit

public enum DoubleSpinnerConstraintsIdentifier {
    
    case height
    case width
    
    public var identifier: String {
        switch self {
        case .height:
            return "DoubleSpinnerHeightConstraint"
        case .width:
            return "DoubleSpinnerWidthConstraint"
        }
    }
    
}

enum DoubleSpinnerCircleType {
    
    case inner
    case outter
    
    var animationKey: String {
        switch self {
        case .inner:
            return "DoubleSpinnerInnerRotation"
        case .outter:
            return "DoubleSpinnerOutterRotation"
        }
    }
    
    var nameKey: String {
        switch self {
        case .inner:
            return "DoubleSpinnerInner"
        case .outter:
            return "DoubleSpinnerOutter"
        }
    }
}

open class DoubleSpinnerView: UIView {
    
    open var spinnerCenterPoint: CGPoint = .zero
    open var circleRadius: CGFloat = 20
    
    //MARK: inner circle params
    open var innerCircleRadius: CGFloat = 0
    open var innerCirclePausedValue: Float = 0
    open var innerCircleLineWidth: Float = 3
    open var innerCircleAnimationDuration: CFTimeInterval = 4
    open var innerCircleLineColor: UIColor = .red
    open var innerCircleRadiusMultiplier: CGFloat = 0.6
    /// this array detemines which length of filled/empty lines will be drawn for inner circle [filled, empty, filled, empty....]
    open var innerCircleDashPattern: [NSNumber] = [118 ,13, 13, 209, 209, 13, 13, 209]
    
    //MARK: //outter circle params
    open var outterCircleRadius: CGFloat = 0
    open var outterCirclePausedValue: Float = 0
    open var outterCircleLineWidth: Float = 3
    open var outterCircleAnimationDuration: CFTimeInterval = 3 //time needed for 1 full circle
    open var outterCircleLineColor: UIColor = .red
    /// this array detemines which length of filled/empty lines will be drawn for outter circle [filled, empty, filled, empty....]
    open var outterCircleDashPattern: [NSNumber] = [118 ,13]
    
    ///add image if you want it to show in the center of spinner
    open var centerImageView: UIImageView = {
      
        let imageView = UIImageView(image: UIImage(named: "bt_icon_scan"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
        
    }()

    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
        self.addImageView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func addImageView() {
        
        self.addSubview(self.centerImageView)
        self.centerImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        self.centerImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        
    }
    
    //MARK: - draws both rings for spinner
    ///- if the center point is not defined, center point will be calculated from width/height constraints (using constraint identifiers: DoubleSpinnerConstraintsIdentifier)
    ///- if you want view to follow width/height constraints need to add identifiers DoubleSpinnerConstraintsIdentifier, or setup Radii manually self.thisIsTestView.innerCircleRadius = 50....
    /// - parameter startAnimation: add this bool if you want to conrol when the animation will start, default is TRUE which means it will start immediately after rings are drawn
    
    open func drawDoubleSpinner(startAnimation: Bool = true) {
        
        //in case that this method is called more than 1 times on DoubleSpinnerView all previous animations and layers are removed
        self.removePreviousSublayers()
        
        self.setupCenterPont()
        
        self.drawInnerCircle()
        self.drawOutterCircle()
        
        
        if startAnimation == true {
            self.addRotatingAnimation()
        }
       
    }
    
    //MARK: - Add rotating animations
    /// - this method will go trough all sublayers and search for shapelayers,
    /// - after that according to layer.name will be applied different animation
    open func addRotatingAnimation() {
        
        for layer in self.layer.sublayers! {
            
            if layer is CAShapeLayer {
                
                if let name = layer.name {
                    
                    if name == DoubleSpinnerCircleType.inner.nameKey {
                        layer.add(self.rotatingAnimation(clockwise: false), forKey: DoubleSpinnerCircleType.inner.animationKey)
                    }
                    if name == DoubleSpinnerCircleType.outter.nameKey {
                        layer.add(self.rotatingAnimation(clockwise: true), forKey: DoubleSpinnerCircleType.outter.animationKey)
                    }
                }
            }
        }
    }
    
    //MARK: Pause animation
    /// - this method will save current state of ration,
    /// - after that animation is removed and layers are set to last position they were in
    /// - when the animation was paused
    open func pauseAnimations() {
        
        guard (self.layer.sublayers != nil) else {  return }
        
        for shapeLayer in self.layer.sublayers! {
            
            if let layer = shapeLayer as? CAShapeLayer {
                
                self.lockLayerToCurrentPosition(layer: layer)
                layer.removeAllAnimations()
            }
        }
    }
    
    fileprivate func lockLayerToCurrentPosition(layer: CAShapeLayer) {
        
        if let layerPresentation = layer.presentation(), let currentRotationOffset = layerPresentation.value(forKeyPath: "transform.rotation.z") as? Float {
            
            if layer.animation(forKey: DoubleSpinnerCircleType.inner.animationKey) != nil {
                self.innerCirclePausedValue = currentRotationOffset
            }
            if layer.animation(forKey: DoubleSpinnerCircleType.outter.animationKey) != nil {
                self.outterCirclePausedValue = currentRotationOffset
            }
            
            layer.transform = CATransform3DMakeRotation(CGFloat(currentRotationOffset), 0, 0, 1)
            
        }
    }
    
    //MARK: Removes all animations and resets position of layers
    open func stopAnimations() {
        
        guard (self.layer.sublayers != nil) else {  return }
        
        for layer in self.layer.sublayers! {
            
            self.innerCirclePausedValue = 0
            self.outterCirclePausedValue = 0
            
            if (layer is CAShapeLayer)
            {
                layer.transform = CATransform3DMakeRotation(0, 0, 0, 1)
                layer.removeAllAnimations()
                
            }
        }
    }
    
    //MARK: - Remove all previously added sublayers
    
    fileprivate func removePreviousSublayers()
    {
        guard (self.layer.sublayers != nil) else {  return }
        
        self.layer.removeAllAnimations()
        
        for layer in self.layer.sublayers! {
            
            if (layer is CAShapeLayer)  {
                
                layer.removeFromSuperlayer()
            }
        }
    }
    

    //MARK: - Draw layers
    func drawInnerCircle() {
        
        self.layer.addSublayer(self.getInnerShapeLayer())
        
    }
    func drawOutterCircle() {
        
        self.layer.addSublayer(self.getOuterShapeLayer())
        
    }

    //MARK: - Setup center point if not previously set
    fileprivate func setupCenterPont() {
        
        if self.spinnerCenterPoint == .zero {
            let calculatedCenterPoint = self.fetchWidthHeightConstraintValues()
            
            if let width = calculatedCenterPoint.width {
                self.spinnerCenterPoint.x = width/2
            } else {
                self.spinnerCenterPoint.x = self.circleRadius
            }
            if let height = calculatedCenterPoint.height {
                self.spinnerCenterPoint.y = height/2
            } else {
                
                self.spinnerCenterPoint.y = self.circleRadius
            }
        }
        
        self.updateCircleRadii()
    }
    
    fileprivate func updateCircleRadii() {
        
        if self.outterCircleRadius == 0 { //case that circle Radius is not set
            if self.spinnerCenterPoint.x < self.spinnerCenterPoint.y {
                self.outterCircleRadius = self.spinnerCenterPoint.x - CGFloat(self.outterCircleLineWidth)
            } else if self.spinnerCenterPoint.x > self.spinnerCenterPoint.y {
                self.outterCircleRadius = self.spinnerCenterPoint.y - CGFloat(self.outterCircleLineWidth)
            } else {
                self.outterCircleRadius = self.spinnerCenterPoint.x - CGFloat(self.outterCircleLineWidth)
            }
        }
        if self.innerCircleRadius == 0 {//case that circle Radius is not set
            self.innerCircleRadius = self.outterCircleRadius*self.innerCircleRadiusMultiplier
        }
        
        self.updateDashPatterns()
    }
    
    open func updateDashPatterns() {
        
        self.updateInnerDashPattern()
        self.updateOutterDashPattern()
        
    }
    
    fileprivate func updateInnerDashPattern() {
        
        var innerDashPattern: [NSNumber] = []
        let oneFourthynthOfCircle = NSNumber(value: self.getCircleArcLength(Radius: Float(self.innerCircleRadius), degreeAngle: 10))
        let oneFourthOfCircle = NSNumber(value: (self.getCircleArcLength(Radius: Float(self.innerCircleRadius), degreeAngle: 90)))
        let oneFifthOfCircle = NSNumber(value: self.getCircleArcLength(Radius: Float(self.innerCircleRadius), degreeAngle: 60))
        innerDashPattern.append(oneFourthynthOfCircle)
        innerDashPattern.append(oneFourthynthOfCircle)
        innerDashPattern.append(oneFourthOfCircle)
        innerDashPattern.append(oneFifthOfCircle)
        innerDashPattern.append(oneFourthynthOfCircle)
        innerDashPattern.append(oneFourthynthOfCircle)
        innerDashPattern.append(oneFourthOfCircle)
        innerDashPattern.append(oneFourthOfCircle)
        
        self.innerCircleDashPattern = innerDashPattern
    }
    
    fileprivate func updateOutterDashPattern() {
        var outterDashPattern: [NSNumber] = []
        let oneFourthynthOfCircle = NSNumber(value: self.getCircleArcLength(Radius: Float(self.outterCircleRadius), degreeAngle: 45)/10)
        let oneFourthOfCircle = NSNumber(value: (self.getCircleArcLength(Radius: Float(self.outterCircleRadius), degreeAngle: 45) - oneFourthynthOfCircle.floatValue))
        //[118 ,13]
        outterDashPattern.append(oneFourthOfCircle)
        outterDashPattern.append(oneFourthynthOfCircle)
        
        self.outterCircleDashPattern = outterDashPattern
        
    }
    
    fileprivate func fetchWidthHeightConstraintValues() -> (width: CGFloat?, height: CGFloat?) {
        
        let height = self.constraintsAffectingLayout(for: .vertical).filter({ $0.identifier ==  DoubleSpinnerConstraintsIdentifier.height.identifier }).first
        let width = self.constraintsAffectingLayout(for: .horizontal).filter({ $0.identifier ==  DoubleSpinnerConstraintsIdentifier.width.identifier }).first
        
        return (width?.constant, height?.constant)
        
    }
    
    
    //MARK: - add rotating animation to drawn layers
    
    fileprivate func rotatingAnimation(clockwise: Bool = true) -> CABasicAnimation {
        
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.duration = clockwise == true ? self.outterCircleAnimationDuration : self.innerCircleAnimationDuration
        animation.repeatDuration = CFTimeInterval(CGFloat.greatestFiniteMagnitude)
        animation.fromValue = clockwise == true ? CGFloat(self.outterCirclePausedValue) : CGFloat(self.innerCirclePausedValue)
        animation.byValue = clockwise ? CGFloat.pi*2 : -CGFloat.pi*2
        animation.isRemovedOnCompletion = false
        return animation
        
    }
    
    
    //MARK: - Methods used for drawing/fetching outter and inner circle
    func getOuterShapeLayer() -> CAShapeLayer {
        
        let shapeLayer = self.drawCircleLayer(beginAngle: 0, endAngle: CGFloat.pi*2, clockwise: true, Radius: self.outterCircleRadius)
        
        shapeLayer.lineWidth =  CGFloat(self.outterCircleLineWidth)
        shapeLayer.lineDashPattern = self.outterCircleDashPattern
        shapeLayer.name = DoubleSpinnerCircleType.outter.nameKey
        shapeLayer.strokeColor = self.outterCircleLineColor.cgColor
        
        return shapeLayer
    }
    
    func getInnerShapeLayer() -> CAShapeLayer {
        
        let shapeLayer = self.drawCircleLayer(beginAngle: 0, endAngle: CGFloat.pi*2, clockwise: false, Radius: self.innerCircleRadius)
        
        shapeLayer.lineWidth =  CGFloat(self.innerCircleLineWidth)
        shapeLayer.lineDashPattern = self.innerCircleDashPattern
        shapeLayer.name = DoubleSpinnerCircleType.inner.nameKey
        shapeLayer.strokeColor = self.innerCircleLineColor.cgColor
        
        return shapeLayer
    }
    
    fileprivate func drawCircleLayer(beginAngle: CGFloat, endAngle: CGFloat, clockwise: Bool = true, Radius: CGFloat = 120) -> CAShapeLayer {
        
        let path = UIBezierPath(arcCenter: CGPoint(x: self.spinnerCenterPoint.x
            , y: self.spinnerCenterPoint.y), radius: Radius, startAngle: beginAngle, endAngle: endAngle, clockwise: clockwise)
        
        return self.getShapeLayer(layerPath: path, isOutter: clockwise)
    }
    

    //MARK: - Get generic shape layer
    fileprivate func getShapeLayer(layerPath: UIBezierPath, isOutter: Bool = true) -> CAShapeLayer {
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.contentsScale = UIScreen.main.scale
        //shapeLayer.shouldRasterize = true
        shapeLayer.path = layerPath.cgPath
        shapeLayer.position = self.spinnerCenterPoint
        shapeLayer.bounds = CGRect(x: 0, y: 0, width: self.spinnerCenterPoint.x*2, height: self.spinnerCenterPoint.y*2)
        shapeLayer.lineCap = "round"
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth = 8
        
        return shapeLayer
    }
    
    //MARK: - Helpers
    
    func getCircleArcLength(Radius: Float, degreeAngle: Float) -> Float {
        
        return (Radius*Float(CGFloat.pi)*degreeAngle)/180
        
    }
}


