//
//  ViewController.swift
//  DoubleSpinnerView
//
//  Created by Dejan Kraguljac on 03/01/2018.
//  Copyright © 2018 Dejan Kraguljac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let thisIsTestView: DoubleSpinnerView = {
        
        let doubleView = DoubleSpinnerView(frame: .zero)
        
        doubleView.circleRadius = UIScreen.main.bounds.size.width/2 - 40
        doubleView.translatesAutoresizingMaskIntoConstraints = false
        
        var innerDashPattern: [NSNumber] = []
        doubleView.outterCircleLineColor = .white
        doubleView.innerCircleDashPattern = innerDashPattern
        doubleView.innerCircleLineColor = .white
        return doubleView
        
    }()
    
    let button: UIButton = {
        
        let button = UIButton(frame: .zero)
        button.setTitle("Stop animation", for: .normal)
        button.setTitle("ContinueAnimation", for: .selected)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.thisIsTestView)
        
        self.thisIsTestView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        
        
        self.thisIsTestView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        
        //if you want view to follow width/height constraints need to add identifiers DoubleSpinnerConstraintsIdentifier, or setup Radiuses manually self.thisIsTestView.innerCircleRadius = 50....
        let width = self.thisIsTestView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.width)
        width.identifier = DoubleSpinnerConstraintsIdentifier.width.identifier
        width.isActive = true
        
        let height = self.thisIsTestView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.height)
        height.identifier = DoubleSpinnerConstraintsIdentifier.height.identifier
        height.isActive = true
        
        self.thisIsTestView.backgroundColor = .black
        self.thisIsTestView.drawDoubleSpinner()
        
        self.view.addSubview(self.button)
        self.button.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        self.button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        self.button.addTarget(self, action: #selector(startStopAnimation), for: .touchUpInside)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc func startStopAnimation(){
        
        self.button.isSelected = !self.button.isSelected
        switch self.button.isSelected {
        case true:
            self.thisIsTestView.pauseAnimations()
        default:
            self.thisIsTestView.addRotatingAnimation()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



}

